
Pod::Spec.new do |s|
  s.name             = 'BViewController'
  s.version          = '0.1.0'
  s.summary          = 'A short description of BViewController.'

  s.description      = 'bvc'
  s.homepage         = 'https://gitee.com/testfor/BViewController'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Vincent' => '1433869783@qq.com' }
  s.source           = { :git => 'https://gitee.com/testfor/BViewController.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'

  s.source_files = 'BViewController/Classes/**/*'

  # s.dependency 'AFNetworking', '~> 2.3'
end
