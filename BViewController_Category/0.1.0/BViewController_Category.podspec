
Pod::Spec.new do |s|
  s.name             = 'BViewController_Category'
  s.version          = '0.1.0'
  s.summary          = 'A short description of BViewController_Category.'

  s.description      = 'bvccategory'
  s.homepage         = 'https://gitee.com/testfor/BViewController_Category'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Vincent' => '1433869783@qq.com' }
  s.source           = { :git => 'https://gitee.com/testfor/BViewController_Category.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'

  s.source_files = 'BViewController_Category/Classes/**/*'

  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'CTMediator'
  
end
