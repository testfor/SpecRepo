
Pod::Spec.new do |s|
  s.name             = 'AViewController'
  s.version          = '0.1.0'
  s.summary          = 'A short description of AViewController.'

  s.description      = 'avc'
  s.homepage         = 'https://gitee.com/testfor/AViewController'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Vincent' => '1433869783@qq.com' }
  s.source           = { :git => 'https://gitee.com/testfor/AViewController.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'

  s.source_files = 'AViewController/Classes/**/*'

  # s.dependency 'AFNetworking', '~> 2.3'
end
