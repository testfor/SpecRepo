
Pod::Spec.new do |s|
  s.name             = 'AViewController_Category'
  s.version          = '0.1.0'
  s.summary          = 'A short description of AViewController_Category.'

  s.description      = 'av_category'
  s.homepage         = 'https://gitee.com/testfor/AViewController_Category'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Vincent' => '1433869783@qq.com' }
  s.source           = { :git => 'https://gitee.com/testfor/AViewController_Category.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'

  s.source_files = 'AViewController_Category/Classes/**/*'

  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'CTMediator'
  
  
end
